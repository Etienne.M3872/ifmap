import React, { useState, useEffect } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  Dimensions,
  Modal,
  AsyncStorage,
} from 'react-native';
import PropTypes from 'prop-types';
import { AntDesign } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Colors from '../../../constants/Colors';

const NotificationItem = ({
  id, type, title, description, isDeletable,
}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [notifVisible, setNotifVisible] = useState(true);
  let icon;
  let color;
  switch (type) {
    case 'Warning':
      icon = 'warning';
      color = Colors.notifYellow;
      break;
    case 'Danger':
      icon = 'exclamationcircle';
      color = Colors.notifRed;
      break;
    case 'Reminder':
      icon = 'clockcircle';
      color = Colors.notifGreen;
      break;
    case 'Info':
      icon = 'questioncircle';
      color = Colors.notifBlue;
      break;
    default:
      icon = 'questioncircle';
      color = Colors.notifWhite;
      break;
  }

  const storeGlobalDelete = async (delId) => {
    try {
      if (isDeletable) {
        const deletedNotifications = JSON.parse(await AsyncStorage.getItem('deletedNotifications'));
        deletedNotifications.push(delId);
        await AsyncStorage.setItem('deletedNotifications', JSON.stringify(deletedNotifications));
      } else {
        // console.log('not deletable');
      }
    } catch (error) {
      await AsyncStorage.setItem('deletedNotifications', JSON.stringify([delId]));
      // console.log('the cache item probably did not exist previously or was corrupted', error);
    }
  };

  // Check if notif was hidden
  const fetchNotifStatus = async (delId) => {
    try {
      const deletedNotifications = JSON.parse(await AsyncStorage.getItem('deletedNotifications'));
      if (deletedNotifications.includes(delId)) {
        return false;
      }
    } catch (error) {
      // console.log('error fetching notif status');
    }
    return true;
  };

  //  Once the component is mounted, check if the notif should be visible
  useEffect(() => {
    let isSubscribed = true;
    fetchNotifStatus(id).then((isVisible) => {
      if (isSubscribed) {
        setNotifVisible(isVisible);
      }
    });
    return () => {
      isSubscribed = false;
    };
  }, [fetchNotifStatus]);

  if (notifVisible) {
    return (
      <View>
        <Modal animationType="slide" transparent visible={modalVisible}>
          <View style={styles.modal}>
            <View style={styles.modalContent}>
              <Text style={styles.notificationTitle}>{title}</Text>
              <Text style={styles.notificationBody}>{description}</Text>
              <Button
                title="OK"
                style={styles.confirmButton}
                onPress={() => {
                  setModalVisible(false);
                  storeGlobalDelete(id);
                  setNotifVisible(false);
                }}
              />
            </View>
          </View>
        </Modal>
        <View style={{ ...styles.notificationIcon, backgroundColor: color }}>
          <TouchableOpacity onPressIn={() => { setModalVisible(true); }}>
            <AntDesign name={icon} size={44} color={Colors.white} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  return <View />;
};

NotificationItem.propTypes = {
  id: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  isDeletable: PropTypes.bool.isRequired,
};

const styles = StyleSheet.create({
  confirmButton: {
    color: Colors.primary,
  },
  modal: {
    alignItems: 'center',
    top: Dimensions.get('window').height * 0.15,
  },
  modalContent: {
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderRadius: 15,
    height: Dimensions.get('window').height * 0.60,
    justifyContent: 'space-evenly',
    padding: 20,
    width: Dimensions.get('window').width * 0.90,
  },
  notificationBody: {
    color: Colors.notifBody,
    fontSize: 18,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  notificationIcon: {
    borderRadius: 7,
    fontSize: 24,
    height: 50,
    marginVertical: 5,
    opacity: 0.9,
    padding: 3,
    textAlign: 'center',
    textAlignVertical: 'center',
    width: 50,
  },
  notificationTitle: {
    color: Colors.notifTitle,
    fontSize: 24,
    textAlign: 'center',
    textAlignVertical: 'top',
  },
});

export default NotificationItem;
