/* eslint-disable import/prefer-default-export */
/*
    This file contains variables from various files
    which influence various things in the application.

    Changing their value will change something in the app
    live after saving.
*/
import Notification from '../models/Notification';

const n1 = new Notification(
  1, 'Versions',
  'Please make sure to check on GitLab regularly for new versions as we implement more features.',
  'Warning',
  2, 1, 1, false, null,
);
const n2 = new Notification(
  2, 'Welcome!',
  'Welcome to IFMAP!\nFor the best experience, please scan pictures by using the photo button when your fruit is centered in the white box and resting on a flat surface.\nThank you for being here.',
  'Info', 1, 1, 1, true, null,
);
const n3 = new Notification(
  3, 'New Features',
  'New features: You can now change the flash of your camera or upload your own pictures from your camera roll.\nWe have also improved the display of the main screen.',
  'Info', 2, 1, 1, true, null,
);
export const NOTIFICATIONS = [n1, n2, n3];
