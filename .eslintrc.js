module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": [
        "airbnb",
        "plugin:react-native/all"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "react-native"
    ],
    "rules": {
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        'global-require': 'off',
        "no-use-before-define": ["error", { "functions": true, "classes": true, "variables": false }],
        "react/forbid-prop-types": "off",
        'no-plusplus': 'off',
        'react-native/no-raw-text': ['error', {'skip': ['DefaultText'] }],
        'linebreak-style': 'off',
        'no-multiple-empty-lines': 'off',
        'no-underscore-dangle': 'off',
        'react/prop-types': 'off',
        'react-native/no-inline-styles': 'off',
        'no-nested-ternary': 'off'
    }
};
