/* eslint-disable react/prefer-stateless-function */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

class DashBoardScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>DashBoardScreen</Text>
      </View>
    );
  }
}
export default DashBoardScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});
