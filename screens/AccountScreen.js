import React from 'react';
import {
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  Alert,
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import HeaderButton from '../components/HeaderButton';

const SettingsScreen = () => (
  <View style={styles.content}>
    <Button
      title="Clear cache"
      onPress={() => {
        AsyncStorage.removeItem('guestMode');
        AsyncStorage.removeItem('deletedNotifications');
        Alert.alert('Cache cleared', 'Your cache has been cleared', [{ text: 'OK' }]);
      }}
    />
  </View>
);

SettingsScreen.navigationOptions = (navData) => ({
  headerTitle: 'Meal Categories',
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
});

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default SettingsScreen;
