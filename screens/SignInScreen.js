/* eslint-disable react/no-this-in-sfc */
import React from 'react';
import {
  View, Button, StyleSheet, AsyncStorage, Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import firebase from 'firebase';
import * as Google from 'expo-google-app-auth';
import Expo from 'expo';
import FBSDK, { LoginManager } from 'react-native-fbsdk';
/* import { createSwitchNavigator } from 'react-navigation';
import LoadingScreen from './LoadingScreen';
import DashBoardScreen from './DashBoardScreen';
const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen: LoadingScreen,
  LoginScreen: LoginScreen,
  DashboardScreen: DashBoardScreen
}); */

const isUserEqual = (googleUser, firebaseUser) => {
  if (firebaseUser) {
    const { providerData } = firebaseUser;
    for (let i = 0; i < providerData.length; i++) {
      if (providerData[i].providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID
        && providerData[i].uid === googleUser.getBasicProfile().getId()) {
        // We don't need to reauth the Firebase connection.
        return true;
      }
    }
  }
  return false;
};
const onSignIn = (googleUser) => {
  // We need to register an Observer on Firebase Auth to make sure auth is initialized.
  const unsubscribe = firebase.auth().onAuthStateChanged((firebaseUser) => {
    unsubscribe();
    console.log('Google Auth Response', googleUser);
    // Check if we are already signed-in Firebase with the correct user.
    if (!isUserEqual(googleUser, firebaseUser)) {
      // let idToken = googleUser.getAuthResponse();
      // Build Firebase credential with the Google ID token.
      const credential = firebase.auth.GoogleAuthProvider.credential(
        googleUser.idToken, googleUser.accessToken,
      );
      // Sign in with credential from the Google user.
      firebase.auth().signInWithCredential(credential)
        .then(() => {
          console.log('user signed in ');
        })
        .catch((error) => {
        // Handle Errors here.
          const errorCode = error.code;
          const errorMessage = error.message;
          // The email of the user's account used.
          const { email } = error;
          // The firebase.auth.AuthCredential type that was used.
          const { credential } = error;
        // ...
        });
    } else {
      console.log('User already signed-in Firebase.');
    }
  }).bind(this);
};
const signInWithGoogleAsync = async () => {
  try {
    const result = await Google.logInAsync({
      behavior: 'app',
      androidClientId: '832408249834-7qg9nmkl8pglhshu3ngp0elkvk298nqj.apps.googleusercontent.com',
      // iosClientId: YOUR_CLIENT_ID_HERE,
      scopes: ['profile', 'email'],
    });

    if (result.type === 'success') {
      this.onSignIn(result);
      console.log('C bizarre!1');
      return result.accessToken;
    }
    console.log('C bizarre!2');
    return { cancelled: true };
  } catch (e) {
    console.log('C bizarre!3');
    isUserEqual();
    onSignIn();
    return { error: true };
  }
};

/* const _fbAuth() {
  LoginManager.logInWithPermissions(['public_profile']).then(function(result) {
    if(result.isCancelled) {
      console.log('Login was cancelled ');
    } else {
      console.log('Login was a success' + result.grantedPermissions.toString());
    }
  });
}; */

const SignInScreen = (props) => {
  const storeGuest = async (isOn) => {
    try {
      await AsyncStorage.setItem('guestMode', isOn);
    } catch (error) {
      Alert.alert(
        'Error',
        'Error occured saving guest mode',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      );
    }
  };


  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <Button
          title="Login with Facebook"
          onPress={() => { this._fbAuth(); }}
        />
        <Button
          title="Login with Google"
          onPress={() => signInWithGoogleAsync()}
        />
        <Button
          title="Continue as guest"
          onPress={() => {
            storeGuest('true');
            props.navigation.navigate('Fruit Scanner');
          }}
        />
      </View>
    </View>
  );
};


SignInScreen.navigationOptions = () => ({
  headerTitle: 'Login',
});

SignInScreen.propTypes = {
  navigation: PropTypes.any.isRequired,
};

const styles = StyleSheet.create({
  buttons: {
    height: 150,
    justifyContent: 'space-between',
    marginTop: 20,
    width: 200,
  },
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default SignInScreen;
